# README #

### Contributors ###

* Konstantin Fickel (author)
* Rainer Endres (improvements)
* Marco Foglar (improvements)
* Tobias Arndt (improvements)
* Simon Loibl (improvements)
* Albert de Wit (improvements)

### Things that might be missing ###
* Min-/Maxterms in the sector KNF/DNF
* The semantic definitions
